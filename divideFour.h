#include <systemc.h>
SC_MODULE(divideFour)
{
  sc_in<bool> a;  // input pin a
  sc_in<bool> b;  // input pin b
  sc_in<bool> c;
  sc_out<bool> o1; // output pin
  sc_out<bool> o2;
  sc_out<bool> ns1;
  sc_out<bool> ns2;
  SC_CTOR(divideFour)    // the ctor
    {
      SC_METHOD(process);
      sensitive << a << b << c;
    }
  void process() {
      o1.write((!a.read() && b.read() && !c.read()) || (a.read() && b.read() && !c.read()) );
      o2.write((!a.read() && !b.read() && c.read()) || (a.read() && !b.read() && c.read()) );
      ns1.write((!a.read() && !b.read() && c.read()) || (!a.read() && b.read() && !c.read()) );
      ns2.write((!a.read() && !b.read() && !c.read()));
  }
};

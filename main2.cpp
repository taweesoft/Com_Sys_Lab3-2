#include <systemc.h>
#include "divideFour.h"
int sc_main(int argc, char *argv[]){
  divideFour check("");

  sc_signal<bool> input[4];

  //1100  = 12
  input[0] = true;
  input[1] = true;
  input[2] = false;
  input[3] = false;
  sc_signal<bool> in1;

  sc_signal<bool> cs1;
  sc_signal<bool> cs2;

  sc_signal<bool> out1;
  sc_signal<bool> out2;
  sc_signal<bool> ns1;
  sc_signal<bool> ns2;

  check.a(in1);
  check.b(cs1);
  check.c(cs2);

  check.o1(out1);
  check.o2(out2);
  check.ns1(ns1);
  check.ns2(ns2);

  sc_trace_file *wf = sc_create_vcd_trace_file("wave");
  // Dump the desired signals
  sc_trace(wf, in1, "input");
  sc_trace(wf, cs1, "cs1");
  sc_trace(wf, cs2, "cs2");
  sc_trace(wf, out1, "out1");
  sc_trace(wf, out2, "out2");
  sc_trace(wf, ns1, "ns1");
  sc_trace(wf, ns2, "ns2");

  cs1 = false;
  cs2 = false;
  for(int i=0 ;i<4;i++){
    in1 = input[i];
    sc_start(1 , SC_NS);
    cout << "INPUT : " << in1 << " " << cs1 << " " << cs2 << endl;
    cout << "OUTPUT : " << out1 << " " << out2 << " " << ns1 << " " << ns2 << endl;
    cs1 = ns1;
    cs2 = ns2;
  }


  if(ns1 && !ns2){
    cout << "This number can divisible by 4" << endl;
  }else{
    cout << "This number can not divisible by 4" << endl;
  }




}
